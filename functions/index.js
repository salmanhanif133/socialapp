const functions = require("firebase-functions");

const app = require("express")();

const { 
    getAllSCreams, 
    postOneScream, 
    getScream, 
    commentOnScream, 
    likeScream, 
    unlikeScream ,
    deleteScream 
} = require('./handlers/screams');

const { signup, login, uploadImage, addUserDetails, getAuthenticatedUser } = require('./handlers/users');

const FBauth = require('./util/fbAuth');

//Screams route
app.get("/screams", getAllSCreams); //GetScream 
app.post("/createScream", FBauth, postOneScream); //PostScream
app.get('/scream/:screamId', getScream);
app.get('/scream/:screamId/', FBauth, deleteScream)
app.get('/scream/:screamId/unlike', FBauth, unlikeScream)
app.get('/scream/:screamId/like', FBauth, likeScream)
app.post('/scream/:screamId/comment', FBauth, commentOnScream)

//users route
app.post("/signup", signup); //Signup 
app.post("/login", login); //Login
app.post("/user/image", FBauth, uploadImage)
app.post('/user', FBauth, addUserDetails);
app.get('/user', FBauth, getAuthenticatedUser)




exports.api = functions.region("asia-east2").https.onRequest(app);
