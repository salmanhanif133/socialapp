const { db, admin } = require('../util/admin');
const config = require('../util/config')
const firebase = require('firebase');
const { validateSignUpData, validateLoginData, reduceUserDetails } = require('../util/validators')

firebase.initializeApp(config);

exports.signup = async (req, res) => {
    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword,
        handle: req.body.handle
    };

    const { valid, errors } = validateSignUpData(newUser)
    if (!valid) return res.status(400).json(errors);

    const defaultImg = 'defaultimage.png'

    try {
        //TODO validate data
        let token, userId, data;
        let doc = await db.doc(`/users/${newUser.handle}`).get();
        if (doc.exists) {
            return res.status(400).json({ handle: `This handle is already taken!` });
        } else {
            data = await firebase
                .auth()
                .createUserWithEmailAndPassword(newUser.email, newUser.password);
        }

        userId = data.user.uid;
        let idToken = await data.user.getIdToken();
        token = idToken;
        const userCredentials = {
            handle: newUser.handle,
            email: newUser.email,
            createdAt: new Date().toISOString(),
            imageUrl: `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${defaultImg}?alt=media`,
            userId
        };
        await db.doc(`/users/${newUser.handle}`).set(userCredentials);
        return res.status(201).json({ token });
    } catch (err) {
        console.error(err);
        if (err.code === "auth/email-already-in-use") {
            return res.status(400).json({ email: "Email is already in use" });
        } else {
            return res.status(500).json({ error: err.code });
        }
    }
}
//Log user in
exports.login = async (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    };

    const { valid, errors } = validateLoginData(user)
    if (!valid) return res.status(400).json(errors);

    try {
        let data = await firebase.auth().signInWithEmailAndPassword(user.email, user.password);
        let token = await data.user.getIdToken()
        return res.json({ token });
    } catch (err) {
        console.error(err);
        if (err.code = "auth/wrong-password") {
            return res.status(403).json({ general: "Wrong credentials, please try agian" })
        } else return res.status(500).json({ error: err.code });
    }
}

//Add user details
exports.addUserDetails = async (req, res) => {
    let userDetails = reduceUserDetails(req.body);
    try {
        await db.doc(`/users/${req.user.handle}`).update(userDetails)
        return res.json({ message: 'Details added succsefully' })
    } catch(err) {
        console.error(err);
        return res.status(500).json({message: err.code})
    }
}

//Get own user details
exports.getAuthenticatedUser = async (req, res) => {
    let userData = {};
    let data;
    try {
        let doc = await db.doc(`users/${req.user.handle}`).get()
        if(doc.exists) {
            userData.credentials = doc.data()
            data = await db.collection('likes').where('userHandle', '==', req.user.handle).get()
        }
        userData.likes = []
        data.forEach(doc => {
            userData.likes.push(doc.data());
        });
        return res.json(userData)
    } catch(err) {
        console.error(err);
        return res.status(500).json({message: err.code})
    }
}

// Upload a profile image for user
exports.uploadImage = (req, res) => {
    const BusBoy = require('busboy');
    const path = require('path');
    const os = require('os');
    const fs = require('fs');
  
    const busboy = new BusBoy({ headers: req.headers });
  
    let imageToBeUploaded = {};
    let imageFileName;
  
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
    //   console.log(fieldname, file, filename, encoding, mimetype);
      if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
        return res.status(400).json({ error: 'Wrong file type submitted' });
      }
      // my.image.png => ['my', 'image', 'png']
      const imageExtension = filename.split('.')[filename.split('.').length - 1];
      // 32756238461724837.png
      imageFileName = `${Math.round(
        Math.random() * 1000000000000
      ).toString()}.${imageExtension}`;
      const filepath = path.join(os.tmpdir(), imageFileName);
      imageToBeUploaded = { filepath, mimetype };
      file.pipe(fs.createWriteStream(filepath));
    });
    busboy.on('finish', () => {
      admin
        .storage()
        .bucket()
        .upload(imageToBeUploaded.filepath, {
          resumable: false,
          metadata: {
            metadata: {
              contentType: imageToBeUploaded.mimetype
            }
          }
        })
        .then(() => {
          const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${
            config.storageBucket
          }/o/${imageFileName}?alt=media`;
          return db.doc(`/users/${req.user.handle}`).update({ imageUrl });
        })
        .then(() => {
          return res.json({ message: 'image uploaded successfully' });
        })
        .catch((err) => {
          console.error(err);
          return res.status(500).json({ error: 'something went wrong' });
        });
    });
    busboy.end(req.rawBody);
  };